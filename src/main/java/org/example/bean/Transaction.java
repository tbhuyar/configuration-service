package org.example.bean;

import lombok.Data;

@Data
public class Transaction {
    int orderId;
    int itemId;
    int orderQty;
    String region;
}
